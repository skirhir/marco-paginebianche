#!/usr/bin/python
from urlparse import urlparse
from lxml.html import parse
import itertools
from Queue import Queue
from threading import Thread, Lock
import time
import urllib2
from sys import argv,exit
import re
import cookielib
import sqlite3
import string
from xlsxwriter.workbook import Workbook
import myemail

#from email_extractor import email_scrapper



base="http://www.paginegialle.it/pgol/4-elettricista"

database = "./database.db"

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
downloader=myemail.myReDownloader(opener)

#SQLITE INITIALIZATION 
db=sqlite3.connect(database)
dblock=Lock()


sql= """
CREATE TABLE IF NOT EXISTS elettricista
(  ID text primary key ,  Name text,  Address text,  Zip_code text,  City text,  Phone text,  www text,  category text,  URL_of_post text,  Number_of_ratings text,  Average_rating text,  Categories text,  Services text,  Activities text,  Other text,  emails text );
"""
db.execute(sql)

blockExtensions=['.css','.xml','.rss','.ico','.js','.gif','.jpg','.jpeg','.png','.bmp','.wmv','.avi','.mp3','.flash','.swf', '.pdf']


#Misc Functions

def my_str(obj):
  try:
    return str(obj)
  except Exception as e:
    return obj

def join_queue(q, timeout=None):
  q.all_tasks_done.acquire()
  try:
    if timeout is None:
      while q.unfinished_tasks:
        q.all_tasks_done.wait()
    elif timeout < 0:
      raise ValueError("'timeout' must be a positive number")
    else:
      endtime = time.time() + timeout
      while q.unfinished_tasks:
        remaining = endtime - time.time()
        if remaining <= 0.0:
          raise Pending
        q.all_tasks_done.wait(remaining)
  finally:
      q.all_tasks_done.release()

def get_field( field, doc ):
  try:
    return [i for i in doc.find_class("dettagli")[0].findall(".//dt") if i.text_content()==field][0].getnext().text_content()
  except Exception as e:
    return ""

def subpage_parse(record):
  try:
    doc=parse(downloader.open(record['URL_of_post'])).getroot()


    doc.make_links_absolute()
    record["Categories"]=get_field( "Categoria", doc)
    record["Services"]=get_field( "Servizi", doc)
    record["Activities"]=get_field( u"Attivit\u00E0", doc)
    record["Other"]=get_field( "Informazioni Generali", doc)

    try:
      record["Number_of_ratings"]=doc.find_class("_noc commenti votes")[0].text_content()
      avg=doc.find_class("voti-commenti")[0].find(".//a").get("title")
      record["Average_rating"]=re.search(': (.*)$',avg).group(1)
    except:
      record["Number_of_ratings"]=""
      record["Average_rating"]=""

  except Exception as e:
    print e, record['URL_of_post'], record['ID']

  sql="INSERT INTO elettricista (%s) VALUES (%s)" % ( string.join(record.keys(),',' ) , string.join(['?']*len(record),',') )

  try:
    c=db.cursor()
    with dblock:
      c.execute( sql ,  tuple([ my_str(value) for value in record.values()] ) )
      db.commit()

  except Exception as e:
    print e

#if len(argv) != 2:
#  print "This script requires a <SITE> page as input"
#  exit()
#
#page=argv[1]

def worker(q):
  db=sqlite3.connect(database)
  c=db.cursor()
  sql=("UPDATE elettricista SET emails = ? WHERE ID = ?")
  myscrapper=myemail.EmailExtractor(ignore=blockExtensions)

  while True:
    try:
      item=q.get()
      if item is None: break
      ( item_id, www )=item
  
      emails=myscrapper.scrap(www)
      if emails:
        with dblock:
          c.execute( sql ,  tuple( [ my_str( string.join(emails,',') ), item_id ] ) )
          db.commit()
        print "finished with", www, item_id 
      else:
        print "No mail found on", www, item_id

    except Exception as e:
      print e

    q.task_done()

  print 'Worker %s Finished'
  q.task_done()
      

q=Queue()
num_threads = 10
for i in xrange(num_threads):
  t=Thread(target=worker, args=(q,))
  t.daemon=True
  t.start()
#Bootstrap

c=db.cursor()


doc=parse(downloader.open(base)).getroot()


per_localita=[ i.get("href") for i in doc.find_class("all_ul")[0].findall(".//a") ]
#per_localita=[ i.get("href") for i in doc.find_class("all_ul")[0].findall(".//a") ][3:4]

is_internal=re.compile("http://www.paginegialle.it")

for localita in per_localita:
  print localita
  page=localita + "?mr=50"
  req = urllib2.Request(page)
  while True:
    try:
      doc=parse(downloader.open(req)).getroot()
      doc.make_links_absolute()
    except Exception as e:
      print e, page

    try:
      for item in doc.find_class("item"):
 
        record={}
        record['ID']=item.get("id")

        #skip repeated
        sql='select * from elettricista where ID = ? ;' 
        with dblock:
          if c.execute(sql, [ record['ID'] ]).fetchall():
            continue

                

        try:
          record['Name']=item.find_class("_lms _noc")[0].text_content()
        except Exception as e:
          record['Name']=""
        print record['Name']
        try:
          record['Address']=item.find_class("street-address")[0].text_content()
        except Exception as e:
          record['Address']=""

        try:
          locality=item.find_class("locality")[0].text_content()
          ( record['Zip_Code'], record['City'] ) = re.match("([0-9]*) (.*)",locality).groups()
        except Exception as e:
          record['Zip_Code']=""
          record['City']=""

        try:
          record['Phone']=item.find_class('tel')[0].text_content().strip().replace('\n',' ')
        except Exception as e:
          record['Phone']=""

        try:
          link=item.find_class('link')[0].find('.//a')
          if re.match("http",link.get("href")):
            record['www']=link.get("href")
          else:
            record['www']=""
        except Exception as e:
          record['www']=""

        try:
          record['category']=string.join([ cat.text_content() for cat in item.find_class('cat')[0].findall('.//a') ] , ' ')
        except Exception as e:
          record['category']=""

        try:
          uri=item.find_class("_lms _noc")[0].get("href")

          if is_internal.match(uri):
            record['URL_of_post']=uri
            subpage_parse(record)
          else:
            record['URL_of_post']=""
            try:
              sql="INSERT INTO elettricista (%s) VALUES (%s)" % ( string.join(record.keys(),',' ) , string.join(['?']*len(record),',') )
              with dblock:
                c.execute( sql ,  tuple([ my_str(value) for value in record.values()] ) )
                db.commit()
            except Exception as e:
              print e

        except Exception as e:
          record['URL_of_post']=""
          try:
            sql="INSERT INTO elettricista (%s) VALUES (%s)" % ( string.join(record.keys(),',' ) , string.join(['?']*len(record),',') )
            with dblock:
              c.execute( sql ,  tuple([ my_str(value) for value in record.values()] ) )
              db.commit()
          except Exception as e:
            print e

  
        #Launch webscrapping thread
        if record['www']:
          q.put( [record['ID'],record['www']] )
         


      print page, q.qsize()
      referer=page
      page=doc.find_class("succ")[0].get("href")
  
      if page == referer:
        break
  
      if page:
        req = urllib2.Request(page)
        req.add_header('Referer', referer)
      else:
        break
  
    except Exception as e:
      break

print "Finished, waiting for threads to end"
for i in range(num_threads):
  q.put(None)

q.join()
#join_queue(q, 60*10 )


#Write excel
sql="select * from elettricista"
result=c.execute(sql)

workbook = Workbook('./test.xlsx')
worksheet = workbook.add_worksheet()

#Write headers
for c,col in enumerate([ x[0] for x in  result.description ]):
  worksheet.write(0, c, col)

#Dump table
for r, row in enumerate(result):
  for c, col in enumerate(row):
    worksheet.write(r+1, c, col)
workbook.close()
