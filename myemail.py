import logging

from urlparse import urlparse
from lxml.html import parse
from lxml.etree import tostring
import itertools
from Queue import Queue
from threading import Thread
import time
import urllib2
from sys import argv,exit
import re
import cookielib
import sqlite3
import string
from collections import deque
import google

from socket import error as SocketError
import errno


logging.basicConfig(level=logging.DEBUG, 
                      format='%(asctime)s %(levelname)s: %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S')


class myReDownloader(object):
  def __init__ (self,opener,retries=3):
    self.opener=opener
    self.retries=retries
  def open(self, target):
    for attempts in xrange(self.retries):
      try:
        return self.opener.open(target)
      except SocketError as e:
        if e.errno != errno.ECONNRESET:
          raise
        pass
    raise



class EmailExtractor(object):
  def __init__( self, opener=urllib2.build_opener() , ignore=[] , soft_limit=10 , hard_limit=100, rec_limit=3 ):
    self.opener=opener

    self.ignore=ignore

    self.soft_limit=soft_limit
    self.hard_limit=hard_limit
    self.rec_limit=rec_limit

  def scrap( self, target=None):
    if target is None:
      target=self.target

    my_job=EmailExtractorJob( target,self.opener, self.ignore, self.soft_limit, self.hard_limit, self.rec_limit)
    return my_job.scrap()


class EmailExtractorJob(object):
  def __init__(self, target, opener=urllib2.build_opener() , ignore=[] , soft_limit=10 , hard_limit=100, rec_limit=3 ):
    logging.debug("Started new job %s" % target)
    self.target=target

    self.downloader=myReDownloader(opener)

    self.ignore=ignore

    self.soft_limit=soft_limit
    self.hard_limit=hard_limit
    self.rec_limit=rec_limit

    self.emails=set()
    self.parsed_uris=[]
    self.uri_queue=deque()
    self.mailsrch = re.compile(r'[\w\-][\w\-\.]+@[\w\-][\w\-\.]+\.[a-zA-Z]{1,4}') 

  def is_valid(self,uri):
#    logging.debug("is_valid %s" % uri)

    if uri is None: return False
    parsed_target=urlparse(self.target)
    parsed_uri=urlparse(uri)

    if parsed_uri.hostname!=parsed_target.hostname: return False

    if not re.match('http', parsed_uri.geturl()): return False


    path=parsed_uri.path.upper()
    for i in ( x.upper() for x in self.ignore):
      if path.endswith(i): return False

    return True
      

  def scrap( self, target=None, depth=0 ):
    if target is None:
      target=self.target

    logging.debug("depth=%i, count=%i, Scrapping %s" % ( depth, len(self.parsed_uris), target) )
    
    try:
      doc=parse(self.downloader.open(target)).getroot()
    except: return self.emails

    doc.make_links_absolute()

    self.emails.update( self.mailsrch.findall( tostring(doc)) )

    self.uri_queue.extendleft( [ x.get("href") for x in doc.findall(".//a") if self.is_valid(x.get("href")) ] )

    self.uri_queue.extend( reversed([ x.get("src") for x in doc.findall(".//iframe") if self.is_valid(x.get("src")) ])) 
    self.uri_queue.extend( reversed([ x.get("src") for x in doc.findall(".//frame") if self.is_valid(x.get("src")) ])) 

    if depth >= self.rec_limit : return self.emails
    while self.uri_queue :
      if len(self.parsed_uris) >  self.hard_limit : return self.emails
      if ( len(self.parsed_uris) >  self.soft_limit ) and  self.emails : return self.emails


      round_target=self.uri_queue.pop()
      if round_target in self.parsed_uris: continue
      self.parsed_uris.append(round_target)
      self.scrap( round_target , depth + 1 )


    #Try with google if nothin else works

    if depth==0 and not self.emails:

      logging.debug("Trying with Google if nothing else works")

      results=google.search("site:%s" % target)

      count=0
      for res in results:
        if res in self.parsed_uris: continue

        self.parsed_uris.append(res)
        count+=1

        self.scrap( res ,depth + 1)

        if count >=self.soft_limit : break
        if self.emails: break


    return self.emails
